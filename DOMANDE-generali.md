Esempio di domanda generale (nell'esame reale, di solito le domande di questo
tipo sono quattro)

1. Discutere i possibili usi di Docker utili a facilitare lo sviluppo in gruppi
   di lavoro complessi.

   Docker è un servizio di virtualizzazione in grado di creare immagini (sistema)
   o container (sistema + processo) contenenti ciò che si sta producendo. Nel caso
   dei gruppi di lavoro complessi, Docker aiuta a risolvere la dependency hell, in
   quanto crea uno snapshot dell'ambiente su cui è stato sviluppato il software,
   fornendo in tal modo a tutti gli utilizzatori un ambiente identico, garantendo
   il corretto funzionamento.
